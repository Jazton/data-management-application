import java.awt.List;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.graphics.Point;


public class PopUpWindow extends Shell {
	
	protected Shell shell;
	protected Display display;
	public String categoryOfAircraft;
	public String aircraft;
	ArrayList<Airplane> Fighter= new ArrayList<Airplane>();
	ArrayList<Airplane> Heavy= new ArrayList<Airplane>();
	ArrayList<Airplane> Helicopter= new ArrayList<Airplane>();
	

	/**
	 * Launch the application.
	 * @param args
	 */
	public PopUpWindow() 
	{		
		
	}
	
	public void run(ArrayList<Airplane> fighterList, ArrayList<Airplane> heavyList, ArrayList<Airplane> helicopterList)
	{
		try {
			display = Display.getDefault();
			
			shell = new PopUpWindow(display, fighterList, heavyList, helicopterList);
			shell.open();
			shell.layout();		
			
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Create the shell.
	 * @param display
	 * @wbp.parser.constructor
	 */
	public PopUpWindow(Display display, ArrayList<Airplane> fighter2, ArrayList<Airplane> heavy2, ArrayList<Airplane> helicopter2) {
		super(display, SWT.SHELL_TRIM);
		Fighter = fighter2;
		Heavy = heavy2;
		Helicopter = helicopter2;
		
		createContents();
		
	}
	

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		
		setText("SWT Application");
		setSize(455, 174);
		setLayout(new GridLayout(2, false));
		
				
		Label lblTypeOfAircraft = new Label(this, SWT.NONE);
		lblTypeOfAircraft.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTypeOfAircraft.setText("Aircraft Category");
		
		final Combo combo = new Combo(this, SWT.NONE);
		
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		combo.add("Fighter");
		combo.add("Heavy");
		combo.add("Helicopter");
		
		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("Type of aircraft");
		
		final Combo combo_1 = new Combo(this, SWT.NONE);
		
		combo_1.setEnabled(false);
		combo_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);
		new Label(this, SWT.NONE);

		
		final Button btnNewButton = new Button(this, SWT.NONE);
		btnNewButton.setEnabled(false);
			
		
		GridData gd_btnNewButton = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_btnNewButton.heightHint = 43;
		gd_btnNewButton.widthHint = 148;
		btnNewButton.setLayoutData(gd_btnNewButton);
		btnNewButton.setText("Add to inventory");
		
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				combo_1.setEnabled(true);
				combo_1.removeAll();
				try {
					ArrayList<Airplane> aircraft = typesOfAircraft(combo.getText());
					for(Airplane s : aircraft )
						combo_1.add(s.type);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
		});
		
		btnNewButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
		
				if(combo.getText().equals("Fighter"))
				{
				
					Fighter.add(new Airplane("Fighter", combo_1.getText()));
				}
				if(combo.getText().equals("Heavy"))
				{
				
					Heavy.add(new Airplane("Heavy", combo_1.getText()));
				}	
				if(combo.getText().equals("Helicopter"))
				{
				
					Helicopter.add(new Airplane("Helicopter", combo_1.getText()));
				}
				dispose();
			}	
			
		});
		
		
		combo_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				btnNewButton.setEnabled(true);
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	
	public ArrayList<Airplane> typesOfAircraft(String type) throws IOException
	{
		Scanner s = new Scanner(new File(type + ".txt"));
		ArrayList<Airplane> list = new ArrayList<Airplane>();
		while (s.hasNext()){
		    list.add(new Airplane(type, s.next()));
		}
		s.close();
		return list;
	}
	
	public String getCategory()
	{
		return categoryOfAircraft;
	}
	
	public String getAircraft()
	{
		return aircraft;
	}
	

}
