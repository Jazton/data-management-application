import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;


public class Airplane 
{
	String category;
	String type;
	String homeSquadron;	
	
	public Airplane(String categoryIn, String typeIn)
	{
		category = categoryIn;
		type = typeIn;
	}
	
	public void setCategory(String cat)
	{
		category = cat;
	}
	
	public void setType(String typeIn)
	{
		type = typeIn;
	}
	
	public void setSquad(String squad)
	{
		homeSquadron = squad;
	}
	
	public boolean equals(Airplane temp)
	{
		return(this.category.equals(temp.category) && this.type.equals(temp.type));
	}
	
	public String toString()
	{
		return(type + " " + category);
	}
	
}